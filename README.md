# callhome

This script allows SSH access to a computer which does not 
have an open port on the internet.

the access is made by a constant SSH connection to a gateway server, 
where the local SSH port is forwarded

The scrips expects that SSH keys have been installed to do the login


## Getting started

Simple installation

```
git clone git@gitlab.com:kjeld.flarup/callhome.git
```

Add an entry to crontab
```
crontab -e
```
Enter crontab line
```
*/10 * * * * /home/user/callhome/callhome mygatewaydomain.com
```

# Usage
First connect to the gateway machine, and then from the gateway machine connect to the desired computer using the gateway

```
ssh mygatewaydomain.com
ssh -p 2222 localhost
```

# Advanced usage
Provided that the gateway machine has an open port on the internet (besides SSH), then this port can be forwarded directly, making login on the gateway unnessecary.
```
*/10 * * * * /home/user/callhome/callhome mygatewaydomain.com 192.168.1.1:2222
```

Usage

```
ssh -p 2222 mygatewaydomain.com
```

